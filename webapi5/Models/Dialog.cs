﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace webApi5.Models
{
    public class Dialog
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }   
        public bool Schedule{ get; set; } 
        public bool Marketing { get; set; }
        public bool Event { get; set; }
        public bool WaterFall { get; set; }
        public DateTime LastOperation { get; set; }
    }
}