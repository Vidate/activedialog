﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using webApi5.Models;
using Microsoft.AspNet.Cors;

namespace webApi5.Controllers
{
    [Route("api/")]
    public class ValuesController : Controller
    {
        private static int count = 0;
        // GET: api/values
        [HttpGet]
        [Route("dialogs")]
        [EnableCors("MyPolicy")]
        public IEnumerable<Dialog> Get(int quantity)
        {
            if (quantity > 1000)
            {
                return null;
            }
            var resutl = new List<Dialog>();
            var random = new Random(DateTime.Now.Second);
            for (int i = 0; i < quantity; i++)
            {
                resutl.Add(new Dialog()
                {
                    Id = random.Next(0, quantity),
                    Schedule = true,
                    Marketing = false,
                    Event = false,
                    WaterFall = true,
                    Name = $"Opertation nr: {i.ToString()}",
                    Description = "Bla bla bla"
                });
            }
            return resutl;
        }
        [HttpGet]
        [Route("GetDiffrentData")]
        [EnableCors("MyPolicy")]
        public IEnumerable<Dialog> GetDiffrentData()
        {
            var resutl = new List<Dialog>();
            resutl.Add(new Dialog()
            {
                Id = 1,
                Schedule = true,
                Marketing = false,
                Event = false,
                WaterFall = false,
                Name = $"Wygrałeś samochód",
                Description = "Tworzy samochód",
                LastOperation = DateTime.Now.AddMinutes(-10)
            });
            resutl.Add(new Dialog()
            {
                Id = 2,
                Schedule = false,
                Marketing = true,
                Event = false,
                WaterFall = false,
                Name = $"Wygrałeś iPhone 50",
                Description = "Wysyła spam",
                LastOperation = DateTime.Now.AddMinutes(-20)

            });
            resutl.Add(new Dialog()
            {
                Id = 3,
                Schedule = true,
                Marketing = true,
                Event = true,
                WaterFall = true,
                Name = $"Spam do użytkowników poczty",
                Description = "Bla bla",
                LastOperation = DateTime.Now.AddMinutes(-40)

            });
            var random = new Random(DateTime.Now.Second);
            for (int i = 0; i < 6; i++)
            {
                resutl.Add(new Dialog()
                {
                    Id = 3 + i,
                    Schedule = random.NextDouble() > 0.5 ? true : false,
                    Marketing = random.NextDouble() > 0.5 ? true : false,
                    Event = random.NextDouble() > 0.5 ? true : false,
                    WaterFall = random.NextDouble() > 0.5 ? true : false,
                    Name = $"This is a random dialog nr: {i}",
                    Description = "Bla bla",
                    LastOperation = DateTime.Now.AddMinutes(-i * 44)
                });
            }

            if (count == 0)
            {
                count = 1;
                return resutl;
            }
            else
            {
                count = 0;
                resutl[0].Schedule = true;
                resutl[0].Marketing = true;
                resutl[0].Event = false;
                resutl[0].WaterFall = true;
                resutl[0].Description = "Wysylanie widadomości";
                resutl[0].LastOperation = DateTime.Now.AddMinutes(-10);
                resutl[1].Schedule = false;
                resutl[1].Marketing = true;
                resutl[1].Event = true;
                resutl[1].WaterFall = false;
                resutl[1].LastOperation = DateTime.Now.AddMinutes(-40);
                resutl[2].Schedule = false;
                resutl[2].Marketing = false;
                resutl[2].Event = false;
                resutl[2].WaterFall = false;
                resutl[2].LastOperation = DateTime.Now.AddMinutes(-20);

            }
            return resutl.OrderByDescending(x => x.LastOperation);
        }

    }
}
