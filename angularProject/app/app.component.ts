
import { Component } from '@angular/core';
import { DialogComponent } from './dialog/dialog.component/dialog.component';

@Component({
  selector: 'my-app',
  template:
  `
  <dialog-component class="container"></dialog-component>
  `,
  directives: [DialogComponent]
})
export class AppComponent {
  
}

