///<reference path='../typings/index.d.ts' />
"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var app_component_1 = require('./app.component');
var http_1 = require('@angular/http');
var dialog_service_1 = require('./dialog/dialog.service/dialog.service');
platform_browser_dynamic_1.bootstrap(app_component_1.AppComponent, [http_1.HTTP_PROVIDERS, dialog_service_1.DialogService]);
$(document).ready(function () {
    console.log('Done');
    var count = 0;
    window.setInterval(function () {
        if (count == 1) {
            $('.iconsStyle').css({ 'color': 'white' });
            count = 0;
        }
        else {
            $('.iconsStyle').css({ 'color': 'black' });
            count = 1;
        }
    }, 2000);
});
//# sourceMappingURL=main.js.map