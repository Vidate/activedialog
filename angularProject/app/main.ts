///<reference path='../typings/index.d.ts' />

import { bootstrap }    from '@angular/platform-browser-dynamic';
import { AppComponent } from './app.component';
import { HTTP_PROVIDERS } from '@angular/http';
import { DialogService } from './dialog/dialog.service/dialog.service'

bootstrap(AppComponent, [HTTP_PROVIDERS, DialogService]);
$(document).ready(function () {
    console.log('Done')
    var count = 0;
    window.setInterval(function () {
        if (count == 1) {
            $('.iconsStyle').css({ 'color': 'white' })
            count = 0;
        } else {
            $('.iconsStyle').css({ 'color': 'black' })
            count = 1;
        }
    }, 2000)
})