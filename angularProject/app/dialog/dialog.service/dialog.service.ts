import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';

@Injectable()
export class DialogService {
    private apiUrl = 'http://localhost:6151/api';  // URL to web API
    constructor(private http: Http) {
    }

    public getDataFromDb(): Observable<Response> {
        return this.http.get(this.apiUrl + "/dialogs?quantity=10");
    }

    public getActiveDialogsFromDb(quantity: number): Observable<Response> {
        return this.http.get(this.apiUrl + "/dialogs?quantity=" + quantity.toString());
    }

    public getDiffrentData(): Observable<Response> {
        return this.http.get(this.apiUrl + "/GetDiffrentData");
    }
}

