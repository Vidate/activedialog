"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var DialogService = (function () {
    function DialogService(http) {
        this.http = http;
        this.apiUrl = 'http://localhost:6151/api'; // URL to web API
    }
    DialogService.prototype.getDataFromDb = function () {
        return this.http.get(this.apiUrl + "/dialogs?quantity=10");
    };
    DialogService.prototype.getActiveDialogsFromDb = function (quantity) {
        return this.http.get(this.apiUrl + "/dialogs?quantity=" + quantity.toString());
    };
    DialogService.prototype.getDiffrentData = function () {
        return this.http.get(this.apiUrl + "/GetDiffrentData");
    };
    DialogService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], DialogService);
    return DialogService;
}());
exports.DialogService = DialogService;
//# sourceMappingURL=dialog.service.js.map