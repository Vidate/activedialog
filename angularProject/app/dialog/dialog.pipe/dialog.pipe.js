"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var DialogPipe = (function () {
    function DialogPipe() {
    }
    DialogPipe.prototype.transform = function (dialogs, filter) {
        if (filter != "" && filter != undefined && dialogs != undefined) {
            return dialogs.filter(function (dialog) { return dialog.Name.toLocaleLowerCase().startsWith(filter.toLocaleLowerCase()); });
        }
        else {
            return dialogs;
        }
    };
    DialogPipe = __decorate([
        core_1.Pipe({ name: 'dialogPipe' }), 
        __metadata('design:paramtypes', [])
    ], DialogPipe);
    return DialogPipe;
}());
exports.DialogPipe = DialogPipe;
//# sourceMappingURL=dialog.pipe.js.map