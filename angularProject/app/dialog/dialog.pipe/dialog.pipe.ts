import { Pipe, PipeTransform } from '@angular/core';
import { DialogModel } from '../dialog.model/dialog.model';
import {Observable} from 'rxjs/Rx';

@Pipe({ name: 'dialogPipe' })
export class DialogPipe implements PipeTransform {

    transform(dialogs: DialogModel[], filter: string): DialogModel[] {

        if (filter != "" && filter != undefined && dialogs != undefined) {
            return dialogs.filter((dialog: DialogModel) => dialog.Name.toLocaleLowerCase().startsWith(filter.toLocaleLowerCase()))
        } else {
            return dialogs;
        }
    }
}