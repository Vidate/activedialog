"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var dialog_service_1 = require('../dialog.service/dialog.service');
var dialog_detail_component_1 = require('./dialog-detail.component/dialog-detail.component');
var dialog_nav_1 = require('../dialog.nav/dialog.nav');
var dialog_pipe_1 = require('../dialog.pipe/dialog.pipe');
var RX_1 = require('rxjs/RX');
var DialogComponent = (function () {
    function DialogComponent(dataBaseService) {
        this.dataBaseService = dataBaseService;
        this.availableUpdates = false;
        this.timeInterval = 10000;
        this.startCheckingForUpdate();
    }
    DialogComponent.prototype.startCheckingForUpdate = function () {
        var _this = this;
        this.refreshSubscription = RX_1.Observable.interval(this.timeInterval).takeWhile(function () { return !_this.availableUpdates; })
            .subscribe(function (val) { return _this.findChanges(); });
    };
    DialogComponent.prototype.findChanges = function () {
        var _this = this;
        this.dataBaseService.getDiffrentData()
            .subscribe(function (data) {
            _this.updateDialogs(data.json());
        }, function (err) { return console.log(err); }, function () {
            console.log('done' + ' ' + _this.timeInterval);
        });
    };
    DialogComponent.prototype.isUpdateRequired = function (dataToCheck) {
        var _loop_1 = function(element) {
            dialog = this_1.dialogs.filter(function (value) { return value.Id == element.Id; })[0];
            if (element === dialog) {
            }
            else {
                return { value: true };
            }
        };
        var this_1 = this;
        var dialog;
        for (var _i = 0, dataToCheck_1 = dataToCheck; _i < dataToCheck_1.length; _i++) {
            var element = dataToCheck_1[_i];
            var state_1 = _loop_1(element);
            if (typeof state_1 === "object") return state_1.value;
        }
        return false;
    };
    DialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataBaseService.getDiffrentData()
            .subscribe(function (data) { return _this.dialogs = data.json(); });
        this.toFilter = "";
    };
    ;
    DialogComponent.prototype.onSelect = function (dialog) {
        this.selectedDialog = dialog;
    };
    DialogComponent.prototype.updateDialogs = function (newData) {
        var _loop_2 = function(newElement) {
            if (this_2.dialogs.some(function (dialog) { return dialog.Id == newElement.Id; })) {
                element = this_2.dialogs.filter(function (value) {
                    return value.Id == newElement.Id;
                })[0];
                element.Schedule = newElement.Schedule;
                element.Marketing = newElement.Marketing;
                element.Event = newElement.Event;
                element.WaterFall = newElement.WaterFall;
                element.Description = newElement.Description;
                element.LastOperation = newElement.LastOperation;
                element.Name = newElement.Name;
            }
        };
        var this_2 = this;
        var element;
        for (var _i = 0, newData_1 = newData; _i < newData_1.length; _i++) {
            var newElement = newData_1[_i];
            _loop_2(newElement);
        }
        this.dialogs.sort(function (a, b) {
            if (a.LastOperation > b.LastOperation) {
                return -1;
            }
            else {
                return 1;
            }
        });
    };
    DialogComponent.prototype.onUpdate = function (value) {
        this.updateDialogs(this.newDialogs);
    };
    DialogComponent.prototype.filterDialogs = function (filter) {
        if (filter != null || filter != "") {
            this.toFilter = filter;
        }
    };
    DialogComponent.prototype.onTimeUpdate = function (value) {
        var _this = this;
        this.timeInterval = value;
        this.refreshSubscription.unsubscribe();
        this.refreshSubscription = RX_1.Observable.interval(this.timeInterval).takeWhile(function () { return !_this.availableUpdates; })
            .subscribe(function (val) { return _this.findChanges(); });
    };
    DialogComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'dialog-component',
            templateUrl: 'dialog.component.html',
            styleUrls: ['dialog.styles.css'],
            providers: [dialog_service_1.DialogService],
            directives: [dialog_detail_component_1.DialogDetailComponent, dialog_nav_1.DialogNavComponent],
            pipes: [dialog_pipe_1.DialogPipe]
        }), 
        __metadata('design:paramtypes', [dialog_service_1.DialogService])
    ], DialogComponent);
    return DialogComponent;
}());
exports.DialogComponent = DialogComponent;
//# sourceMappingURL=dialog.component.js.map