"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var DataBaseService_1 = require('../DataBase/DataBaseService');
var dialog_detail_1 = require('../dialog-detail/dialog.detail');
var dialog_nav_1 = require('../dialog-nav/dialog.nav');
var dialog_pipe_1 = require('../dialog-pipe/dialog.pipe');
var Rx_1 = require('rxjs/Rx');
var DialogsComponent = (function () {
    function DialogsComponent(dataBaseService) {
        this.dataBaseService = dataBaseService;
        this.availableUpdates = false;
        this.timeInterval = 5000;
        this.startCheckingForUpdate();
    }
    DialogsComponent.prototype.startCheckingForUpdate = function () {
        var _this = this;
        Rx_1.Observable.interval(this.timeInterval).takeWhile(function () { return !_this.availableUpdates; })
            .subscribe(function (val) { return _this.findChanges(); });
    };
    DialogsComponent.prototype.findChanges = function () {
        var _this = this;
        this.dataBaseService.getDiffrentData()
            .subscribe(function (data) {
            _this.newDialogs = data.json();
            _this.availableUpdates = _this.isUpdateRequired(_this.newDialogs);
        });
    };
    DialogsComponent.prototype.isUpdateRequired = function (dataToCheck) {
        var _loop_1 = function(element) {
            dialog = this_1.dialogs.filter(function (value) { return value.Id == element.Id; })[0];
            if (element === dialog) {
            }
            else {
                return { value: true };
            }
        };
        var this_1 = this;
        var dialog;
        for (var _i = 0, dataToCheck_1 = dataToCheck; _i < dataToCheck_1.length; _i++) {
            var element = dataToCheck_1[_i];
            var state_1 = _loop_1(element);
            if (typeof state_1 === "object") return state_1.value;
        }
        return false;
    };
    DialogsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataBaseService.getDiffrentData()
            .subscribe(function (data) { return _this.dialogs = data.json(); });
        this.toFilter = "";
        // this.dataBaseService.getDataFromDb().subscribe((data)=> this.dialogs = <Dialog[]>data.json());
    };
    ;
    //   onKey(event:any) {
    //     //    console.log(event.target.value);
    //       this.dialogQuantity =event.target.value
    //       this.updateStates();
    //       // changes
    //   }
    //   updateStates(){
    //          this.dataBaseService.getActiveDialogsFromDb(this.dialogQuantity)
    //        .subscribe((data)=> this.dialogs = <Dialog[]>data.json());
    //   }
    DialogsComponent.prototype.onSelect = function (dialog) {
        this.selectedDialog = dialog;
    };
    DialogsComponent.prototype.updateDialogs = function (newData) {
        var _loop_2 = function(newElement) {
            if (this_2.dialogs.some(function (dialog) { return dialog.Id == newElement.Id; })) {
                element = this_2.dialogs.filter(function (value) {
                    return value.Id == newElement.Id;
                })[0];
                //  this.replaceDialog(newData.indexOf(newElement),this.dialogs.indexOf(element));
                element.Schedule = newElement.Schedule;
                element.Marketing = newElement.Marketing;
                element.Event = newElement.Event;
                element.WaterFall = newElement.WaterFall;
                element.Description = newElement.Description;
                element.LastOperation = newElement.LastOperation;
                element.Name = newElement.Name;
            }
        };
        var this_2 = this;
        var element;
        for (var _i = 0, newData_1 = newData; _i < newData_1.length; _i++) {
            var newElement = newData_1[_i];
            _loop_2(newElement);
        }
        this.dialogs.sort(function (a, b) {
            if (a.LastOperation > b.LastOperation) {
                return -1;
            }
            else {
                return 1;
            }
        });
        this.availableUpdates = false;
        this.startCheckingForUpdate();
    };
    //  replaceDialog(newIndex:number,oldIndex:number){
    //  }
    DialogsComponent.prototype.onUpdate = function (value) {
        this.updateDialogs(this.newDialogs);
    };
    DialogsComponent.prototype.filterDialogs = function (filter) {
        console.log(filter + " was type");
        if (filter != null || filter != "") {
            this.toFilter = filter;
        }
    };
    DialogsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'dialogs-component',
            templateUrl: 'dialogs.component.html',
            styleUrls: ['dialogs.style.css'],
            providers: [DataBaseService_1.DataBaseService],
            directives: [dialog_detail_1.DialogDetailComponent, dialog_nav_1.DialogNavComponent],
            pipes: [dialog_pipe_1.DialogPipe]
        }), 
        __metadata('design:paramtypes', [DataBaseService_1.DataBaseService])
    ], DialogsComponent);
    return DialogsComponent;
}());
exports.DialogsComponent = DialogsComponent;
//# sourceMappingURL=dialogs.component.js.map