import { Component, OnInit } from '@angular/core';
import { DialogModel } from '../dialog.model/dialog.model';
import { DialogService } from '../dialog.service/dialog.service';
import { DialogDetailComponent } from './dialog-detail.component/dialog-detail.component';
import { DialogNavComponent } from '../dialog.nav/dialog.nav';
import { DialogPipe } from '../dialog.pipe/dialog.pipe'

import {Observable, Subscription } from 'rxjs/RX';

@Component({
    moduleId: module.id,
    selector: 'dialog-component',
    templateUrl: 'dialog.component.html',
    styleUrls: ['dialog.styles.css'],
    providers: [DialogService],
    directives: [DialogDetailComponent, DialogNavComponent],
    pipes: [DialogPipe]
})
export class DialogComponent implements OnInit {
    public toFilter: string;
    public dialogs: DialogModel[];
    public selectedDialog: DialogModel;
    public timeInterval: number;
    public availableUpdates: boolean;
    private newDialogs: DialogModel[];
    private refreshSubscription: Subscription;

    constructor(private dataBaseService: DialogService) {
        this.availableUpdates = false;
        this.timeInterval = 10000;
        this.startCheckingForUpdate();
    }

    startCheckingForUpdate() {
        this.refreshSubscription = Observable.interval(this.timeInterval).takeWhile(() => !this.availableUpdates)
            .subscribe(val => this.findChanges());
    }

    findChanges() {
        this.dataBaseService.getDiffrentData()
            .subscribe((data) => {
                this.updateDialogs(<DialogModel[]>data.json());
            }, err => console.log(err),
            () => {
                console.log('done' + ' ' + this.timeInterval);
            });
    }

    isUpdateRequired(dataToCheck: DialogModel[]): boolean {
        for (let element of dataToCheck) {
            var dialog: DialogModel = this.dialogs.filter((value: DialogModel) => value.Id == element.Id)[0];
            if (element === dialog) {
            } else {
                return true;
            }
        }
        return false;
    }

    ngOnInit() {
        this.dataBaseService.getDiffrentData()
            .subscribe((data) => this.dialogs = <DialogModel[]>data.json());
        this.toFilter = "";
    };

    onSelect(dialog: DialogModel) {
        this.selectedDialog = dialog;
    }

    updateDialogs(newData: DialogModel[]) {
        for (let newElement of newData) {
            if (this.dialogs.some(dialog => dialog.Id == newElement.Id)) {
                var element: DialogModel = this.dialogs.filter((value: DialogModel) =>
                    value.Id == newElement.Id)[0];
                element.Schedule = newElement.Schedule;
                element.Marketing = newElement.Marketing;
                element.Event = newElement.Event;
                element.WaterFall = newElement.WaterFall;
                element.Description = newElement.Description;
                element.LastOperation = newElement.LastOperation;
                element.Name = newElement.Name;
            }
        }
        this.dialogs.sort((a, b) => {
            if (a.LastOperation > b.LastOperation) {
                return -1;
            } else {
                return 1;
            }
        });
    }

    onUpdate(value: boolean) {
        this.updateDialogs(this.newDialogs);
    }

    filterDialogs(filter: string) {
        if (filter != null || filter != "") {
            this.toFilter = filter;
        }
    }

    onTimeUpdate(value: number) {
        this.timeInterval = value;
        this.refreshSubscription.unsubscribe();
        this.refreshSubscription = Observable.interval(this.timeInterval).takeWhile(() => !this.availableUpdates)
            .subscribe(val => this.findChanges());
    }
}