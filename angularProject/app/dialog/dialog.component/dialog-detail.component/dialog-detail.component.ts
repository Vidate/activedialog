import { Component, Input } from '@angular/core';
import { DialogModel } from '../../dialog.model/dialog.model';

@Component({
    moduleId: module.id,
    selector: 'dialog-detail',
    templateUrl: 'dialog-detail.component.html',
    styleUrls: ['dialog-detail.styles.css']
})

export class DialogDetailComponent  {
    @Input() dialogDetail: DialogModel;
    constructor(){}

     time(date: Date): string{
         return new Date(date.toString()).toLocaleString();
     }
}