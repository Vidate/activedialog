export class DialogModel {
    Id: number;
    Name: string;
    Description: string;
    Schedule: boolean;
    Marketing: boolean;
    Event: boolean;
    WaterFall: boolean;
    LastOperation: Date;
}