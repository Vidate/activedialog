"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var DialogNavComponent = (function () {
    function DialogNavComponent() {
        this.onTimeUpdate = new core_1.EventEmitter();
        this.onUpdate = new core_1.EventEmitter();
        this.filterEvent = new core_1.EventEmitter();
        this.minValue = 10;
        this.maxValue = 36000;
        this.currentValue = 10;
    }
    DialogNavComponent.prototype.refreshTimeUpdate = function () {
        this.onTimeUpdate.emit(this.currentValue * 1000);
    };
    DialogNavComponent.prototype.update = function () {
        this.onUpdate.emit(true);
    };
    DialogNavComponent.prototype.find = function () {
        this.filterEvent.emit(this.toSearch);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], DialogNavComponent.prototype, "availableUpdates", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], DialogNavComponent.prototype, "onTimeUpdate", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], DialogNavComponent.prototype, "onUpdate", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], DialogNavComponent.prototype, "filterEvent", void 0);
    DialogNavComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'dialog-nav',
            templateUrl: 'dialog.nav.html',
            styleUrls: ['dialog.nav.style.css']
        }), 
        __metadata('design:paramtypes', [])
    ], DialogNavComponent);
    return DialogNavComponent;
}());
exports.DialogNavComponent = DialogNavComponent;
//# sourceMappingURL=dialog.nav.js.map