import { Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'dialog-nav',
    templateUrl: 'dialog.nav.html',
    styleUrls: ['dialog.nav.style.css']
})
export class DialogNavComponent {
    constructor() {
        this.minValue = 10;
        this.maxValue = 36000;
        this.currentValue = 10;
    }

    public minValue: number;
    public maxValue: number;

    public toSearch: string;
    public currentValue: number;

    @Input() availableUpdates: boolean;

    @Output() onTimeUpdate: EventEmitter<number> = new EventEmitter<number>();
    @Output() onUpdate: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() filterEvent: EventEmitter<string> = new EventEmitter<string>();

    refreshTimeUpdate() {
        this.onTimeUpdate.emit(this.currentValue * 1000);
    }

    update() {
        this.onUpdate.emit(true);
    }

    find() {
        this.filterEvent.emit(this.toSearch);
    }
}